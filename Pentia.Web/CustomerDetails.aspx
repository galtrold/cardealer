﻿<%@ Page Title="Customer details" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustomerDetails.aspx.cs" Inherits="Pentia.Web.CustomerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <p></p>

    <div class="row">
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Customer details</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label">Name</label>
                        <p class="form-control-static"><%= Customer != null ? Customer.Name ?? "" : "" %></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Surname</label>
                        <p class="form-control-static"><%= Customer != null ? Customer.Surname ?? "" : "" %></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Address</label>
                        <address>
                            <%= Customer != null && Customer.Address != null ? Customer.Address.Street ?? "" : "" %>
                            <%= Customer != null && Customer.Address != null ? Customer.Address.Number ?? "" : "" %>
                            <br>
                            <%= Customer != null && Customer.Address != null ? Customer.Address.Zip ?? "" : "" %>
                            <%= Customer != null && Customer.Address != null ? Customer.Address.City ?? "" : "" %>
                            <br>
                            <%= Customer != null && Customer.Address != null ? Customer.Address.Country ?? "" : "" %>
                        </address>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Birthday</label>
                        <p class="form-control-static">
                            <%= Customer != null && Customer.Birthday != null ? Customer.Birthday.BirthDate.ToLongDateString() ?? "" : "" %>
                        </p>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Customer Purchases</h4>
                </div>
                <div class="panel-body">

                    <asp:Repeater runat="server" ID="purchaseRepeater">
                        <HeaderTemplate>
                            <table class="table ">
                                <thead>
                                    <tr>
                                        <th>Car</th>
                                        <th>Model</th>
                                        <th>Paid</th>
                                        <th>Sold by</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><asp:Label runat="server" Text='<%# Eval("Car.Make") %>' /></td>
                                <td><asp:Label runat="server" Text='<%# Eval("Car.Model") %>' /></td>
                                <td><asp:Label runat="server" Text='<%# Eval("Paid.Amount") %>' /> <asp:Label runat="server" Text='<%# Eval("Paid.Currency") %>' /></td>
                                <td><asp:Label runat="server" Text='<%# Eval("SoldBy.Name") %>' /></td>
                                <td><asp:Label runat="server" Text='<%# Eval("OrderDate", "{0:d}") %>' /></td>
                            </tr>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
