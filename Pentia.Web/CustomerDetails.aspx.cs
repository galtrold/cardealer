﻿using System;
using System.Collections.Generic;
using Pentia.Application.Services;
using Pentia.DataAccess.Repositories;
using Pentia.Domain.Models;
using Pentia.Domain.Services;

namespace Pentia.Web
{
    public partial class CustomerDetails : System.Web.UI.Page
    {
        public Customer Customer { get; set; }
        public IEnumerable<CarPurchase> CustomerPurchases { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            int customerId;
            if (!string.IsNullOrEmpty(Request.QueryString["customerid"]) && int.TryParse(Request.QueryString["customerid"], out customerId))
            {
                var customerService = new CustomerService(new CustomerRepository(), new CarPurchaseRepository());
                var purchaseService = new PurchaseService(new CarPurchaseRepository());

                Customer = customerService.GetCustomerById(customerId);
                purchaseRepeater.DataSource =  purchaseService.GetPurchaesByUserId(customerId);
                purchaseRepeater.DataBind();


            }
        }
    }
}