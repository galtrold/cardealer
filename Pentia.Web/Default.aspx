﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Pentia.Web.Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">



    <div class="row">
        <div class="col-xs-4">
            <h3>Customers</h3>
        </div>
        <div class="col-xs-8">
            <div class="form-horizontal" style="margin-top: 20px;">
                <div class="form-group">
                    <div class="col-sm-5">
                        <asp:DropDownList ID="filterType" runat="server" CssClass="form-control" />
                    </div>
                    <div class="col-sm-5">
                         <input id="filterValue" runat="server" class="form-control" />
                    </div>
                    <div class="col-sm-2">
                        <asp:Button OnClick="ApplyFilter" runat="server" CssClass="btn btn-default" Text="Updater" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:Repeater ID="customerRepeater" runat="server">
        <HeaderTemplate>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Surname</th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td style="width: 30px;">
                    <asp:Label runat="server" ID="Label3" Text='<%# Eval("Id") %>' />
                </td>
                <td>
                    <asp:HyperLink runat="server" NavigateUrl='<%# String.Format("~/CustomerDetails.aspx?customerid={0}", Eval("Id")) %>'>
                        <asp:Label runat="server" ID="Label1" Text='<%# Eval("Name") %>' />
                    </asp:HyperLink>

                </td>
                <td>
                    <asp:Label runat="server" ID="Label2" Text='<%# Eval("Surname") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>

</asp:Content>
