﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Ajax.Utilities;
using Pentia.Application.Services;
using Pentia.DataAccess.Repositories;
using Pentia.Domain.Models;
using Pentia.Domain.Services;

namespace Pentia.Web
{
    public partial class Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                LoadCustomers();


                // Filter types
                string[] itemNames = Enum.GetNames(typeof (CustomerQueryType));
                var itemValues = Enum.GetValues(typeof (CustomerQueryType));

                for (int i = 0; i < itemNames.Length; i++)
                {
                    filterType.Items.Add(new ListItem(itemNames[i], itemValues.GetValue(i).ToString()));
                }
            }

        }

        private void LoadCustomers()
        {
            var customerService = new CustomerService(new CustomerRepository(), new CarPurchaseRepository());
            var customers = customerService.All();
            customerRepeater.DataSource = customers;
            customerRepeater.DataBind();
        }

        protected void ApplyFilter(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(filterValue.Value))
            {
                LoadCustomers();
            }
            else
            {
                var selectedValue = filterType.SelectedValue;
                var selectedFilter = (CustomerQueryType) Enum.Parse(typeof (CustomerQueryType), selectedValue);
                var customerService = new CustomerService(new CustomerRepository(), new CarPurchaseRepository());
                var result = customerService.Query(selectedFilter, filterValue.Value);
                customerRepeater.DataSource = result;
                customerRepeater.DataBind();
            }
            
        }
    }
}