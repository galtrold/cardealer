﻿using System;
using System.Runtime.Remoting.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pentia.Domain;
using Pentia.Domain.Interfaces;

namespace Pentia.Test.UnitTests
{
    [TestClass]
    public class BirthdayTest
    {
        [TestMethod]
        public void Test_that_age_is_calculated_correctly_the_day_before_birthday()
        {
            // Arrange
            
            IDateTime now = new TestDate(new DateTime(2014, 08, 29, 0, 0, 0));
            var birthDate = new DateTime(1979, 08, 30, 0, 0, 0);
            var birthday = new Birthday(birthDate, now);

            // Act
            var age = birthday.Age();

            // Assert
            Assert.IsTrue(age == 34, "Persons age should be 34 and is calculated to {0}", age);
        }

        [TestMethod]
        public void Test_that_age_is_calculated_correctly_on_the_birthday()
        {



            // Arrange
            IDateTime now = new TestDate(new DateTime(2014, 08, 30));
            var birthDate = new DateTime(1979, 08, 30);
            var birthday = new Birthday(birthDate, now);

            // Act
            var age = birthday.Age();

            // Assert
            Assert.IsTrue(age == 35, "Persons age should be 35 and is calculated to {0}", age);
        }


        // Test class for IDateTime
        class TestDate : IDateTime
        {
            private readonly DateTime _now;

            public TestDate(DateTime now)
            {
                _now = now;
            }

            public DateTime Now { get { return _now; } }

            public DateTime MinValue
            {
                get { return DateTime.MinValue; }
            }
        }


    }
}