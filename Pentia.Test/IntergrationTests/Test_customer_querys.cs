﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pentia.Application.Services;
using Pentia.Domain;
using Pentia.Domain.Models;
using Pentia.Domain.Models.CustomerQueries;

namespace Pentia.Test.IntergrationTests
{
    [TestClass]
    public class Test_customer_querys
    {
        [TestMethod]
        public void Testing_filter_customer_by_name()
        {
            // Arrange 
            var customerService = new CustomerService(new DummyCustomerRepository(), new DummyPurchaseRepository());
            var searchTarget = "Christian";


            // Act
            IEnumerable<Customer> customerResult = customerService.Query(CustomerQueryType.Name, searchTarget);

            // Assert
            Assert.IsTrue(customerResult.Count() == 1);
            Assert.IsTrue(customerResult.First().Name == searchTarget);
        }


       

    }

    public class DummyCustomerRepository : ICustomerRepository
    {
        public IQueryable<Customer> GetAll()
        {
            var customers = new List<Customer>();

            customers.Add(new Customer()
            {
                Address = new Address() { Street = "Langegade", Number = "6", Zip = "2100", Country = "Denmark", City = "Copenhagen" },
                Birthday = new Birthday(new DateTime(1979, 08, 30), new LocalDateTime()),
                Created = DateTime.Now,
                Name = "Christian",
                Surname = "Hansen"
            });

            customers.Add(new Customer()
            {
                Address = new Address() { Street = "Langegade", Number = "6", Zip = "2100", Country = "Denmark", City = "Copenhagen" },
                Birthday = new Birthday(new DateTime(1979, 08, 30), new LocalDateTime()),
                Created = DateTime.Now,
                Name = "Jens",
                Surname = "Petersen"
            });
            customers.Add(new Customer()
            {
                Address = new Address() { Street = "Langegade", Number = "6", Zip = "2100", Country = "Denmark", City = "Copenhagen" },
                Birthday = new Birthday(new DateTime(1979, 08, 30), new LocalDateTime()),
                Created = DateTime.Now,
                Name = "Gunner",
                Surname = "Nu Hansen"
            });

            return customers.AsQueryable();
        }

        public Customer GetById(int id)
        {
            throw new System.NotImplementedException();
        }
    }

    public class DummyPurchaseRepository : IPurchaseRepository
    {
        public IQueryable<CarPurchase> GetAll()
        {
            throw new NotImplementedException();
        }

        public IQueryable<CarPurchase> GetAllByUserId(int it)
        {
            throw new NotImplementedException();
        }
    }

}