﻿using System.Collections.Generic;
using System.Linq;
using Pentia.DataAccess.Repositories;
using Pentia.Domain.Models;
using Pentia.Domain.Models.CustomerQueries;

namespace Pentia.Application.Services
{
    public class PurchaseService : IPurchaseService
    {
        private readonly IPurchaseRepository _purchaseRepository;

        public PurchaseService(IPurchaseRepository purchaseRepository)
        {
            _purchaseRepository = purchaseRepository;
        }

        public IEnumerable<CarPurchase> GetPurchaesByUserId(int id)
        {
            var carPurchases = _purchaseRepository.GetAllByUserId(id);

            return carPurchases.ToList();
        }
    }
}