﻿using System.Collections.Generic;
using System.Linq;
using Pentia.Application.Factories;
using Pentia.Domain.Models;
using Pentia.Domain.Models.CustomerQueries;

namespace Pentia.Application.Services
{
    public class CustomerService : ICustomerService
    {
        private ICustomerRepository _customerRepository;
        private IPurchaseRepository _purchaseRepository;

        public CustomerService(ICustomerRepository customerRepository, IPurchaseRepository purchaseRepository)
        {
            _customerRepository = customerRepository;
            _purchaseRepository = purchaseRepository;
        }

        public IEnumerable<Customer> Query(CustomerQueryType queryType, string queryParameter)
        {
            var facotry = new CustomerQueryFactory(_customerRepository, _purchaseRepository);

            var customerQuery = facotry.QueryFactory(queryType);

            var customers = customerQuery.Query(queryParameter);

            var result = customers.ToList();

            return result;
        }

        public Customer GetCustomerById(int id)
        {
            var customer = _customerRepository.GetById(id);
            return customer;
        }


        public IEnumerable<Customer> All()
        {
            var customers = _customerRepository.GetAll();

            return customers.ToList();
        }
    }
}