﻿using System.Linq;
using Pentia.Domain.Models;
using Pentia.Domain.Models.CustomerQueries;
using Pentia.Domain.Services;

namespace Pentia.Application.CustomerQueries
{
    public class QueryByCustomerName : ICustomerQuery
    {
        private readonly ICustomerRepository _repository;

        public QueryByCustomerName(ICustomerRepository repository)
        {
            _repository = repository;
        }


        public IQueryable<Customer> Query(string name)
        {
            return _repository.GetAll().Where(p => p.Name == name);
        }

        
    }
}