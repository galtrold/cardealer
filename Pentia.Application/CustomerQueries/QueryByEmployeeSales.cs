﻿using System.Linq;
using Pentia.Domain.Models;
using Pentia.Domain.Models.CustomerQueries;
using Pentia.Domain.Services;

namespace Pentia.Application.CustomerQueries
{
    public class QueryByEmployeeSales : ICustomerQuery
    {
        private readonly IPurchaseRepository _repository;

        public QueryByEmployeeSales(IPurchaseRepository repository)
        {
            _repository = repository;
        }

        public IQueryable<Customer> Query(string salesName)
        {
            IQueryable<CarPurchase> purchases = _repository.GetAll().Where(p => p.SoldBy.Name == salesName);

            var customers = purchases.Select(p => p.Customer).Distinct();

            return customers;

        }
    }
}