﻿using System.Linq;
using Pentia.Domain.Models;
using Pentia.Domain.Models.CustomerQueries;
using Pentia.Domain.Services;

namespace Pentia.Application.CustomerQueries
{
    public class QueryByBoughtCarModel : ICustomerQuery
    {
        private readonly IPurchaseRepository _repository;

        public QueryByBoughtCarModel(IPurchaseRepository repository)
        {
            _repository = repository;
        }

        public IQueryable<Customer> Query(string carModel)
        {
            IQueryable<CarPurchase> purchases = _repository.GetAll().Where(p => p.Car.Model == carModel);

            var customers = purchases.Select(p => p.Customer).Distinct();

            return customers;

        }
    }
}