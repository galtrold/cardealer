﻿using System.Linq;
using Pentia.Domain.Models;
using Pentia.Domain.Models.CustomerQueries;
using Pentia.Domain.Services;

namespace Pentia.Application.CustomerQueries
{
    public class QueryByCustomerStreet  : ICustomerQuery
    {
        private readonly ICustomerRepository _repository;

        public QueryByCustomerStreet(ICustomerRepository repository)
        {
            _repository = repository;
        }

        public IQueryable<Customer> Query(string street)
        {
            return _repository.GetAll().Where(p => p.Address.Street == street);
        }
    }
}