﻿using System.Linq;
using Pentia.Domain.Models;
using Pentia.Domain.Models.CustomerQueries;
using Pentia.Domain.Services;

namespace Pentia.Application.CustomerQueries
{
    public class QueryByBoughtCarMake : ICustomerQuery
    {
        private readonly IPurchaseRepository _repository;

        public QueryByBoughtCarMake(IPurchaseRepository repository)
        {
            _repository = repository;
        }

        public IQueryable<Customer> Query(string carMake)
        {
            IQueryable<CarPurchase> purchases = _repository.GetAll().Where(p => p.Car.Make == carMake);

            var customers = purchases.Select(p => p.Customer).Distinct();

            return customers;

        }
    }
}