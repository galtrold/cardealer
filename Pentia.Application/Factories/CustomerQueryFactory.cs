﻿using Pentia.Application.CustomerQueries;
using Pentia.DataAccess.Repositories;
using Pentia.Domain.Models;
using Pentia.Domain.Models.CustomerQueries;
using Pentia.Domain.Services;

namespace Pentia.Application.Factories
{
    public class CustomerQueryFactory
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IPurchaseRepository _purchaseRepository;

        public CustomerQueryFactory(ICustomerRepository customerRepository, IPurchaseRepository purchaseRepository)
        {
            _customerRepository = customerRepository;
            _purchaseRepository = purchaseRepository;
        }

        public ICustomerQuery QueryFactory(CustomerQueryType queryType)
        {
            switch (queryType)
            {
                case CustomerQueryType.Name:
                    return new QueryByCustomerName(_customerRepository);
                case CustomerQueryType.Street:
                    return new QueryByCustomerStreet(_customerRepository);
                case CustomerQueryType.BoughtCarMake:
                    return new QueryByBoughtCarMake(_purchaseRepository);
                case CustomerQueryType.BoughtCarModel:
                    return new QueryByBoughtCarModel(_purchaseRepository);
                case CustomerQueryType.Employee:
                    return new QueryByEmployeeSales(_purchaseRepository);
                default:
                    return null;

            }
        }

    }
}