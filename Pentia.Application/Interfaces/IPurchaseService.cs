﻿using System.Collections.Generic;
using Pentia.Domain.Models;

namespace Pentia.Application.Services
{
    public interface IPurchaseService
    {
        IEnumerable<CarPurchase> GetPurchaesByUserId(int id);
    }
}