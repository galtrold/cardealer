﻿using System.Collections.Generic;
using Pentia.Domain.Models;

namespace Pentia.Application.Services
{
    public interface ICustomerService
    {
        IEnumerable<Customer> Query(CustomerQueryType queryType, string queryParameter);
        Customer GetCustomerById(int id);
        IEnumerable<Customer> All();
    }
}