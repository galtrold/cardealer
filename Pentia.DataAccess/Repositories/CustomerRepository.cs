﻿using System;
using System.Diagnostics;
using System.Linq;
using Pentia.Domain.Models;
using Pentia.Domain.Models.CustomerQueries;

namespace Pentia.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private PentiaContext _context;


        public CustomerRepository()
        {
            _context = new PentiaContext();
        }


        public IQueryable<Customer> GetAll()
        {
            IQueryable<Customer> customers;

            try
            {
                customers = _context.Customers.AsQueryable();
            }
            catch (Exception exception)
            {
                customers = null;
                Debug.Write(exception);

            }

            return customers;

        }

        public Customer GetById(int id)
        {
            Customer customer;


            try
            {
                customer = _context.Customers.Include("Address").FirstOrDefault(p => p.Id == id);
            }
            catch (Exception exception)
            {
                customer = null;
                Debug.Write(exception);

            }


            return customer;


        }
    }
}