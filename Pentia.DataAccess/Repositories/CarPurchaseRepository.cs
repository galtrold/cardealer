﻿using System;
using System.Diagnostics;
using System.Linq;
using Pentia.Domain.Models;
using Pentia.Domain.Models.CustomerQueries;

namespace Pentia.DataAccess.Repositories
{
    public class CarPurchaseRepository : IPurchaseRepository
    {
        private readonly PentiaContext _context;

        public CarPurchaseRepository()
        {
            _context = new PentiaContext();
        }

        public IQueryable<CarPurchase> GetAll()
        {
            IQueryable<CarPurchase> carPurchases;
            

            try
            {
                carPurchases = _context.CarPurchases.AsQueryable();
            }
            catch (Exception exception)
            {
                carPurchases = null;
                Debug.Write(exception);
            }


            return carPurchases;
        }

        public IQueryable<CarPurchase> GetAllByUserId(int userId)
        {
            IQueryable<CarPurchase> carPurchases;
            try
            {
                carPurchases = _context.CarPurchases.Include("Car").Include("SoldBy").Where(c => c.Customer.Id == userId);
            }
            catch (Exception exception)
            {
                carPurchases = null;
                Debug.Write(exception);

            }
            return carPurchases;

        }
    }
}