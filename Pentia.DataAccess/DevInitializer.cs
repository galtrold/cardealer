﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Pentia.Domain;
using Pentia.Domain.Interfaces;
using Pentia.Domain.Models;

namespace Pentia.DataAccess
{
    public class DevInitializer : DropCreateDatabaseAlways<PentiaContext>
    {
        public IDateTime DateTime { get; set; }
        protected override void Seed(PentiaContext context)
        {
            // Create random generator
            var rand = new Random();
            
            // Setup car makes and models.
            var makes = new[] {"BMW", "Skoda", "Saab", "Volvo", "Audi"};
            var models = new[] {"A", "B", "C", "D", "E"};

            // First and Lastnames
            var firstNames = new[] {"Kim", "Allan", "Morten", "Ask", "Gunner", "Sally", "Rebeccas", "Tine", "Signe", "Ditte"};
            var lastNames = new[] {"Hansen", "Jensen", "Baxter", "Mortensen", "Gates", "Balmar", "Breinbjerg", "Martinussen", "Petersen", "Bondgaard"};


            DateTime = new LocalDateTime();

            // Create addresses
            var addrs = new[]
            {
                new Address() {Street = "Langegade", Number = "6", Zip = "2100", Country = "Denmark", City = "Copenhagen"},
                new Address() {Street = "Smallegade", Number = "3", Zip = "2100", Country = "Denmark", City = "Copenhagen"},
                new Address() {Street = "Odinsgade", Number = "62", Zip = "2100", Country = "Denmark", City = "Copenhagen"},
                new Address() {Street = "Damgårds Alle", Number = "54", Zip = "2100", Country = "Denmark", City = "Copenhagen"},
                new Address() {Street = "Kongshøjen", Number = "76", Zip = "2100", Country = "Denmark", City = "Copenhagen"},
                new Address() {Street = "Aarhusvej", Number = "1", Zip = "2100", Country = "Denmark", City = "Copenhagen"},
                new Address() {Street = "Tværgade", Number = "56", Zip = "2100", Country = "Denmark", City = "Copenhagen"},
                new Address() {Street = "Vestergade", Number = "123", Zip = "2100", Country = "Denmark", City = "Copenhagen"}
            };

            // Create employeis/staff/salespersons
            var employees = new[]
            {
                new Employee()
                {
                    Name = "Jimmy",
                    JobTitle = "Salesman",
                    Address = addrs[0],
                    Salary = new Money(38000, "DKK")
                },
                new Employee()
                {
                    Name = "Brian",
                    JobTitle = "Senior Salesman",
                    Address = addrs[1],
                    Salary = new Money(40000, "DKK")
                },
                new Employee()
                {
                    Name = "Peter",
                    JobTitle = "Manager",
                    Address = addrs[2],
                    Salary = new Money(42000, "DKK")
                }
            };
            context.Employees.AddRange(employees);

            var cars = new List<Car>();
            var customers = new List<Customer>();
            var purchases = new List<CarPurchase>();

            // Create CARS, PURCHASES and CUSTOMERS
            for (int i = 0; i < 1000; i++)
            {
                var randMake = rand.Next(0, 4);
                var randModel = rand.Next(0, 4);
                var randAddr = rand.Next(0, 7);
                var randFirstName = rand.Next(0, 9);
                var randLastName = rand.Next(0, 9);
                var randsoldBy = rand.Next(0, 2);
                var randPrice = rand.Next(20, 40);
                var randPaiedPrice = rand.Next(20, 40);
                var randPurchaseYear = rand.Next(1995, 2014);


                var car = new Car()
                {
                    Color = "Red",
                    Extras = new List<string>() {"Aircondition"},
                    Make = makes[randMake],
                    Model = models[randModel],
                    RecommendedPrice = new Money(randPrice*10000, "DKK")
                };
                cars.Add(car);

                var customer = new Customer()
                {
                    Address = addrs[randAddr],
                    Birthday = new Birthday(new DateTime(1979, 08, 30), this.DateTime),
                    Created = DateTime.Now,
                    Name = firstNames[randFirstName],
                    Surname = lastNames[randLastName]
                };
                customers.Add(customer);

                var purchase = new CarPurchase()
                {
                    Car = car,
                    Customer = customer,
                    OrderDate = new DateTime(randPurchaseYear, 08, 15),
                    Paid = new Money(randPaiedPrice*10000, "DKK"),
                    SoldBy = employees[randsoldBy]
                };
                purchases.Add(purchase);

                


            }

            context.Cars.AddRange(cars);
            context.Customers.AddRange(customers);
            context.CarPurchases.AddRange(purchases);

            context.SaveChanges();


            base.Seed(context);
        }


    }
}