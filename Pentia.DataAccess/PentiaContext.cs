﻿using System.Data.Entity;
using Pentia.Domain;
using Pentia.Domain.Models;

namespace Pentia.DataAccess
{
    public class PentiaContext : DbContext
    {

        public DbSet<Customer> Customers { get; set; }

        public DbSet<CarPurchase> CarPurchases { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Car> Cars { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.ComplexType<Money>();
            modelBuilder.ComplexType<Birthday>().Ignore(p => p.Datetime);

            

            Database.SetInitializer(new DevInitializer());


            base.OnModelCreating(modelBuilder);
        }
    }
}