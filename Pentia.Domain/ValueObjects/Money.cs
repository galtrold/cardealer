﻿using System.Runtime.InteropServices;

namespace Pentia.Domain
{
    public class Money
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }

        public Money()
        {
        }

        public Money(decimal amount, string currency)
        {
            Amount = amount;
            Currency = currency;
        }

        public override string ToString()
        {
            return Amount.ToString() + Currency;
        }

        public Money AddMoney(decimal amount)
        {
            return new Money(Amount + amount, Currency);
        }
        public Money SubtractMoney(decimal amount)
        {
            return new Money(Amount - amount, Currency);
        }
    }
}