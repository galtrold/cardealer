﻿using System;
using Pentia.Domain.Interfaces;

namespace Pentia.Domain
{
    public class Birthday : IBirthday
    {
        public DateTime BirthDate { get; set; }
        public IDateTime Datetime { get; set; }

        public Birthday()
        {
        }

        public Birthday(DateTime birthDate, IDateTime datetime)
        {
            BirthDate = birthDate;
            Datetime = datetime;
        }

        public int Age()
        {
            int age = Datetime.Now.Year - BirthDate.Year;
            if (BirthDate > Datetime.Now.AddYears(-age))
                age--;
            return age;
        }

    }
}