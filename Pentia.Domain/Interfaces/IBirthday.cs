﻿using System;

namespace Pentia.Domain.Interfaces
{
    public interface IBirthday
    {
        DateTime BirthDate { get; }
        int Age();
    }
}