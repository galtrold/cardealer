using System.Collections.Generic;
using System.Linq;

namespace Pentia.Domain.Models.CustomerQueries
{
    public interface IPurchaseRepository
    {
        IQueryable<CarPurchase> GetAll();

        IQueryable<CarPurchase> GetAllByUserId(int it);

    }
}