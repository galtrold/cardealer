﻿using System;

namespace Pentia.Domain.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get;}


        DateTime MinValue { get; }
    }
}