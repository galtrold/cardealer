﻿using System.Linq;
using Pentia.Domain.Models;

namespace Pentia.Domain.Services
{
    public interface ICustomerQuery
    {
        IQueryable<Customer> Query(string param);
    }
}