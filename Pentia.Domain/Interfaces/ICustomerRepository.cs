using System.Linq;

namespace Pentia.Domain.Models.CustomerQueries
{
    public interface ICustomerRepository
    {
        IQueryable<Customer> GetAll();

        Customer GetById(int id);
    }
}