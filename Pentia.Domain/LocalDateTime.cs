﻿using System;
using Pentia.Domain.Interfaces;

namespace Pentia.Domain
{
    public class LocalDateTime : IDateTime
    {
        public DateTime Now { get { return DateTime.Now; } }
        public DateTime MinValue { get { return DateTime.MinValue; } }
    }
}