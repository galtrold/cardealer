﻿namespace Pentia.Domain.Models
{
    public abstract class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Address Address { get; set; }
    }
}