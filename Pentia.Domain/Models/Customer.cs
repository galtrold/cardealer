﻿using System;

namespace Pentia.Domain.Models
{
    public class Customer : Person
    {
        public string Surname { get; set; }
        public Birthday Birthday { get; set; }

        public DateTime Created { get; set; }

    }
}