﻿using System.Collections.Generic;

namespace Pentia.Domain.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string Make { get; set; }

        public string Model { get; set; }

        public string Color { get; set; }

        public IEnumerable<string> Extras { get; set; }

        public Money RecommendedPrice { get; set; }

        public string Speed { get; set; }
    }
}