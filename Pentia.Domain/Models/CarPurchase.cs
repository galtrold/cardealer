﻿using System;

namespace Pentia.Domain.Models
{
    public class CarPurchase
    {
        public int Id { get; set; }
        public Customer Customer { get; set; }

        public Car Car { get; set; }

        public DateTime OrderDate { get; set; }

        public Money Paid { get; set; }

        public Employee SoldBy { get; set; }

         
    }
}