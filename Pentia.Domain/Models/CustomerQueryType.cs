﻿namespace Pentia.Domain.Models
{
    public enum CustomerQueryType
    {
        Name = 0,
        Street = 1,
        BoughtCarMake = 2,
        BoughtCarModel = 3,
        Employee = 4
    }
}