﻿namespace Pentia.Domain.Models
{
    public class Employee : Person
    {
        public Money Salary { get; set; }

        public string JobTitle { get; set; }
    }
}